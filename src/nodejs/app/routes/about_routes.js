bodyParser = require('body-parser').json();
const express = require("express");
const bp = require("body-parser");
const txtToJSON = require("txt-file-to-json");
const app = express();
  
const urlencodedParser = bp.urlencoded({extended: false});

module.exports = function (app) {
    app.get('/about', (request, response) => {
        console.log("ABOUT GET");
        var result = [
            {
                "text": "Fedusiv Egor"
            },
            {
                "text" : "19 years old"
            },
            {
                "text" : "Education: 1st year itis bachelor degree"
            },
            {  
                "text" : "Experience:0 days, 0 hours, 0 minutes in industry"
            },
            {
                "text" :  "Skills:any language helloworld programm"
            }
            ];
            
        response.send(JSON.stringify(result));
    });

    app.get('/requests', (request, response) => {
        console.log("REQ GET");
        var result = [];
        const fs = require('fs');

        try {
            const data = fs.readFileSync('log.txt', 'UTF-8');
            const lines = data.split(/\r?\n/);
            lines.forEach((line) => {
                result.push({"text": line});
            });
        } catch (err) {
            console.error(err);
        }
        response.send(JSON.stringify(result));
    });

    app.post("/request", urlencodedParser, function (request, response) {
        if(!request.body) return response.sendStatus(400);
        console.log(request.body);
        var fs = require('fs');
        var s = `${request.body.userName} ${request.body.userAge}` + '\r\n';
        fs.appendFile('log.txt', s, function (err) {
          if (err) {
            // append failed
            console.log("ERROR");
          } else {
            // done
            console.log("DONE");
          }
        })
        response.send("Sended");
    });

};
