package db;

import db.models.Mentor;
import db.models.Student;
import db.repositories.StudentRepository;
import db.repositories.StudentRepositoryJdbcImpl;

import java.sql.*;
import java.util.List;

public class Main {

    private static final String PATH = "jdbc:sqlite:students.db";

    public static void main(String[] args) throws SQLException {
        Connection connection = DriverManager.getConnection(PATH);
        StudentRepository studentRepository = new StudentRepositoryJdbcImpl(connection);
        List<Student> students = studentRepository.findAll();
        for(Student s : students) {
            System.out.println(s);
            for(Mentor m : s.getMentors())
                System.out.println(m);
            System.out.println("================================================");
        }
    }
}
