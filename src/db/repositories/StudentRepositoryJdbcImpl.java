package db.repositories;

import db.models.Mentor;
import db.models.Student;

import java.sql.*;
import java.util.*;

import java.util.List;

public class StudentRepositoryJdbcImpl implements StudentRepository {

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from student where id = ";
    private static final String SQL_SELECT_BY_AGE = "select * from student where age = ";
    private static final String SQL_INSERT_INTO_STUDENT = "insert into " +
            "student(first_name, last_name, age, group_number) values (?, ?, ?, ?)";
    private static final String SQL_SELECT_ALL = "select * from student";
    private static final String SQL_UPDATE = "update student set first_name = ?, last_name = ?, age = ?, " +
            "group_number = ? where id = ?";
    private static final String SQL_FINDALL_STUDENT_MENTOR = "select student.*, mentor.id as m_id, " +
            "mentor.first_name as m_fn, mentor.last_name as m_ln from student left join mentor on\n" +
            "    mentor.student_id = student.id;";

    private Connection connection;

    public StudentRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Student> findAllByAge(int age) {
        Statement statement = null;
        ResultSet result = null;
        List<Student> students = new ArrayList<>();
        try {
            statement = connection.createStatement();
            result = statement.executeQuery(SQL_SELECT_BY_AGE + age);
            while (result.next()) {
                students.add(new Student(
                        result.getLong("id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getInt("age"),
                        result.getInt("group_number")
                ));
            }
            return students;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    
    @Override
    public List<Student> findAll() {
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            Map<Long, Student> students = new HashMap<>();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SQL_FINDALL_STUDENT_MENTOR);
            while (resultSet.next()) {
                Student tempS = new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getInt("age"),
                        resultSet.getInt("group_number"));
                Mentor tempM = new Mentor(
                        resultSet.getLong("m_id"),
                        resultSet.getString("m_fn"),
                        resultSet.getString("m_ln"));
                if (students.containsKey(tempS.getId())) {
                    students.get(tempS.getId()).addMentor(tempM);
                } else {
                    tempS.addMentor(tempM);
                    students.put(tempS.getId(), tempS);
                }
            }
            return new ArrayList(students.values());
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    @Override
    public Student findById(Long id) {
        Statement statement = null;
        ResultSet result = null;

        try {
            statement = connection.createStatement();
            result = statement.executeQuery(SQL_SELECT_BY_ID + id);
            if (result.next()) {
                return new Student(
                        result.getLong("id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getInt("age"),
                        result.getInt("group_number")
                );
            } else return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    @Override
    public void save(Student entity) {
        PreparedStatement preparedStatement = null;
        Statement statement = null;
        ResultSet result = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_INSERT_INTO_STUDENT);
            preparedStatement.setString(1, entity.getFirstName());
            preparedStatement.setString(2, entity.getLastName());
            preparedStatement.setInt(3, entity.getAge());
            preparedStatement.setInt(4, entity.getGroupNumber());

            preparedStatement.execute();
            statement = connection.createStatement();
            result = statement.executeQuery(SQL_SELECT_ALL);
            Long id = null;
            while(result.next()) id = result.getLong("id");
            entity.setId(id);
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }

        }
    }

    @Override
    public void update(Student entity) {
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_UPDATE);
            preparedStatement.setString(1, entity.getFirstName());
            preparedStatement.setString(2, entity.getLastName());
            preparedStatement.setInt(3, entity.getAge());
            preparedStatement.setInt(4, entity.getGroupNumber());
            preparedStatement.setLong(5, entity.getId());
            preparedStatement.execute();

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

}
