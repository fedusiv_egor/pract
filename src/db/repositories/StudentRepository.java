package db.repositories;

import db.models.Student;

import java.util.List;

public interface StudentRepository extends CrudRepository<Student> {
    List<Student> findAllByAge(int age);
}
